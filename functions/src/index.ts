import * as functions from 'firebase-functions';
import * as soap from 'soap';
import * as admin from 'firebase-admin';

admin.initializeApp();

const apiUrl = 'http://api.neocron-game.com:8100/PublicInterface?wsdl';
const sessionUrl =
  'http://api.neocron-game.com:8100/SessionManagement?wsdl';
const apiKey = '53113cd5-4cf4-48ef-88d0-2b909139802b';
const envelope = {
  authenticationKey: apiKey,
};

async function EnsureSession () {
  const client = await soap.createClientAsync(sessionUrl);
  console.log(client.describe());
  const doc = await admin.firestore().collection('token').doc('token').get();
  const tokenObj = doc.data();
  if (tokenObj && tokenObj.token) {
    const env = {
      token: tokenObj.token
    }
    const result = await client.IsSessionValidAsync(env);
    console.log(result);
    const sessionIsValid = (result[0].IsSessionValidResult.Value === 'true');
    if (!sessionIsValid) {
      console.log("we do a login");
      return doLogin(client);
    }
    console.log(tokenObj.token);
    return Promise.resolve(tokenObj.token);
  }
  return doLogin(client);
}

async function doLogin (client: any) {
  const response = await client.AuthenticationKeyLoginAsync(envelope);
  console.log(response);
  const token = response[0].AuthenticationKeyLoginResult.Token;
  if (token) {
    console.log("we have a token: " + token)
    await admin.firestore().collection('token').doc('token').set({ token: token });
    return Promise.resolve(token);
  }
  return Promise.reject(false);
}

exports.scheduledFunction = functions.pubsub
  .schedule('every 10 minutes')
  .onRun(async (context) => {
    const token = await EnsureSession();
    if (!token) {
      console.log("SOMEHOW NO SESSION EXISTS");
      return null;
    }

    const apiClient = await soap.createClientAsync(apiUrl);

    const envelopeToken = {
      token: token,
      server: 'Titan',
    };
    const result = await apiClient.GetOutpostStateAsync(envelopeToken);
    console.log(result);
    return result[0].GetOutpostStateResult.Outposts.ExtendedOutpost.forEach(
      async (outpost: any) => {
        await admin.firestore().collection('clans').doc(outpost.Clan.Name.replace(/\s/g, '')).set(outpost.Clan);
        return admin.firestore().collection('outposts').doc(outpost.Id).set(outpost, { merge: true });
      }
    );
  });

  exports.scheduledKillStatistics = functions.pubsub
  .schedule('every 10 minutes')
  .onRun(async (context) => {
    const token = await EnsureSession();
    if (!token) {
      console.log("SOMEHOW NO SESSION EXISTS");
      return null;
    }

    console.log("EVERYTHING VALID");
    console.log(token);

    const apiClient = await soap.createClientAsync(apiUrl);

    console.log(apiClient.describe());
    const envelopeToken = {
      token,
      server: 'Titan',
    };
    const result = await apiClient.GetKillStatisticsAsync(envelopeToken);

    return result[0].GetKillStatisticsResult.KillStatistics.DetailedKillStatistic.forEach(
      async (kill: any) => {
        const date = Date.parse(kill.TimeStamp);
        return admin.firestore().collection('killEvents').doc(date.toString()).set(kill);
      }
    );
  });



  exports.updateOutpost = functions.firestore
  .document('outposts/{outpostId}')
  .onUpdate((change, context) => {
    const newValue = change.after.data();
    const previousValue = change.before.data();
    console.log(newValue);
    console.log(previousValue);

    if (previousValue && newValue && previousValue.Clan.Name !== newValue.Clan.Name) {
      const transRef = admin.firestore().collection('outposts').doc(context.params.outpostId);

      return transRef.update({ FormerOwner: previousValue.Clan.Name });
    }
    return null;
    // perform desired operations ...
  });