export interface Kill {
    id?: string;
    weapon: string;
    world: string;
    timestamp: Date;
    killer: Player;
    target: Player;
    outpost: string;
}

export interface Player {
    clan: string;
    name: string;
    class: string;
    faction: string;
    profession: string;
    kills?: number;
}

export interface Clan {
    name: string;
    members?: Player[];
    kills?: number;
    best: Player;
    faction: string;
}

export interface OutpostStatistic {
    name: string;
    clan: string;
    faction: string;
    best: Player;
    kills?: number;
}
